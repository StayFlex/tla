package tla_interpretation;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class Perso {

    int PO;
    int PV;
    String State;

    public Perso() {
        this.PV = 20;
        this.PO = 0;
        this.State = "Normal";
    }
    
    
    public void changeState(String newState) {
    	this.State = newState;
    }
    /*
     * State:
     * Poison -> lose 1 PV each you travel to a new room
     * Confuse -> lose a random piece of inventory or 1 PO ( ratio 80% chance to lose 1 PO)
     */
    public void updGold(int upd) {
    	this.PO += upd;
    }
    
    public void updHealth(int upd) {
    	this.PV += upd;
    }
    
    public void healState() {
    	this.State = "Normal";
    }
    
    public void gold(int value) {
    	this.PO+=value;
    	if( this.PO < 0 ) this.PO=0;
    }
    
    public void life(int value) {
    	this.PV+=value;
    }
    
    public int getPv() {
    	return this.PV;
    }
    public int getPO() {
    	return this.PO;
    }
    
   public String getSTATE() {
	   return this.State;
   }
   public void setPV(int pv) {
   	this.PV=pv;
   }
}


