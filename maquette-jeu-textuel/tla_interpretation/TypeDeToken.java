package tla_interpretation;

public enum TypeDeToken {
	proposition,
	debutAventure,
	finAventure,
	titre,
	debutLieu,
	finLieu,
	description,
	choix,
	intLieu,
	numeroLieu,
	Perso,
	Inventory,
	Si,
	leftPar,
	rightPar,
	add,
	minus,
	leftAcc,
	rightAcc,
	inventaire,
	commat,
	strVal,
	perso
}

/*
 * 
 * debutAventure = Lance le lieu n°1 debut
	finAventure = ........ fin
	titre = nom de page
	debutLieu entrer
	finLieu sortie
	description = texte
	choix = proposition choisie
	Lint = lieu du choix
	nextLine = saut de ligne
 * */
