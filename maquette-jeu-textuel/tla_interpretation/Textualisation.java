package tla_interpretation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Textualisation {

	
	private HashMap<Integer, Lieu> lieux = new HashMap<>();

    private String titre;
    
    private String fileName;
    
   private String numeroLieu = "";


    public static Textualisation startAventure(String fileName) {
    	return new Textualisation(fileName);
    }

    private Textualisation(String fileName) {
    	this.fileName=fileName;
    	initAventure(fileName);
    }

    private static String nextToken(Scanner scanner) {
    	String ligne;
    	while (scanner.hasNextLine()) {
    		ligne = scanner.nextLine();
    		if( ! ligne.isEmpty()) {
    			return ligne;
    		}
    	}
    	return "" ;
    }
    
    public void initAventure(String file) {
        try {

        	String Path = new java.io.File("").getAbsolutePath();
        	Scanner scanner = new Scanner(new File(Path+"\\tla_interpretation\\"+file));
            String line = nextToken(scanner);
           
            switch (Token.valueOf(line)) {
                case debutAventure -> Story(scanner);
			default -> throw new IllegalArgumentException("L'histoire doit commencer par le token debutAventure");
            }
        } catch (FileNotFoundException e) {
        	System.out.println("Le fichier " + file + " n'a pas été trouvé.");
        }
    }
    
    

    public void Story(Scanner scanner) {
    	HashMap<Integer, Lieu> lieux = new HashMap<>();
        while (scanner.hasNextLine()) {
        	String token = scanner.nextLine();
           if(! token.isEmpty()) {
	        	switch (Token.valueOf(token)) {
	                case titre:
	                    titre = scanner.nextLine();
	                    break;
	                //case inventory:
	                	//App.inventory = interpretationInventaire(token);
	                case debutLieu:
	                	lieux = interpreterLieu(scanner );
	                	break;
	                case finAventure:
	                    System.out.println("L'aventure \"" + this.titre + "\" s'est bien chargée");
	                    break;
				default:
					break;
	            }
           }
        }
        this.lieux = lieux;
    }


    public ArrayList<ObjectAdventure> interpretationInventaire(Scanner scanner) {
    	
    	ArrayList <ObjectAdventure> inventoryInit = new ArrayList<ObjectAdventure>();
    	String object = scanner.nextLine();
    	 while (scanner.hasNextLine()) {
    		 object = ""+scanner;
    		 inventoryInit.add(new ObjectAdventure(object));
    		 //vérifier les objet en doublons
    	 }
    	
    	return inventoryInit;
    	
    }
    
    public HashMap<Integer, Lieu> interpreterLieu(Scanner scanner) {
        HashMap<Integer, Lieu> lieux = new HashMap<>();
        while (scanner.hasNextLine()) {
        	String token = scanner.nextLine();
        	if(! token.isEmpty()) {

	        	switch (Token.valueOf(token)) {
		        	
	        		case finLieu:
	                    return lieux;    
		        	default:
		                    numeroLieu = "";
		                    String description = "";
		                    List<Proposition> propositions = new ArrayList<Proposition>();
		                    boolean isLieuFinished = false;
		
		                    while (!isLieuFinished && scanner.hasNextLine()) {
		                        if(! token.isEmpty()) {
	
			                        switch (Token.valueOf(token)) {
			                            case numeroLieu:
			                                numeroLieu = token.split(":")[1].trim();
			                                System.out.println("Numero Lieu ici: '"+Integer.parseInt(numeroLieu));
			                                break;
			                            case description:
			                                description = scanner.nextLine();
			                                break;  
			                            case inventaire:
			                            	System.out.println("Numero Lieu: '"+Integer.parseInt(numeroLieu)+"' == '"+App.numeroLieuActuel);
			                            	if(Integer.parseInt(numeroLieu) == App.numeroLieuActuel){
			                            	System.out.println("Token inventaire");
			                            	interpreterInventory(token.split(":")[1].trim());
			                            	}
			                            	break;
			                            case perso:
			                            	if(Integer.parseInt(numeroLieu) == App.numeroLieuActuel){
			                            	System.out.println("Token perso");
			                            	interpreterPerso(token.split(":")[1].trim());
			                            	}
			                            	break;
			                            case proposition:
			                                propositions = interpreterPropositions(scanner);
			                            case finLieu:
			                                isLieuFinished = true;
			                                break;
			                        }
		                        }
		                        token = scanner.nextLine();
		                    }
	                    Lieu lieu = new Lieu( description, propositions);
	                    lieux.put(Integer.parseInt(numeroLieu), lieu);
	                    break;
	                
	            }
        	 }
        }
        return lieux;
    }
    
    private void interpreterInventory(String ligne) {
    	
    	
    	System.out.println("ligne inventaire : "+ligne);
    	//ligne=ligne.split(":")[1];
    	System.out.println("Inventaire Ajout:");
    	String object = "";
    	boolean sign = true;
    	for(int i=0; i<ligne.length(); i++) {
    		String token=ligne.substring(i, i + 1);
    		if(! token.isEmpty()) {
	    		switch(Token.valueOf(token)) {
	    		case commat:
	    			inventoryUpdate(sign, object);
	    			object ="";
	    			break;
	    		case add:
	    			sign=true;
	    			break;
	    		case minus:
	    			sign=false;
	    			break;
	    		default:
	    			object += token;
	    			System.out.println(object);
	    			break;
	    		
	    		}
	    		
    		}
    	}
    	if(!object.equals("")) {
    		inventoryUpdate(sign, object);
    	}
    }
   
    private void inventoryUpdate(boolean sign, String Object) {
        boolean inexistingObject = true;
        System.out.println("L'object à update est :"+Object);
        while(inexistingObject) {
            for(int i = 0; i<App.inventory.size(); i++) {

                System.out.println("'"+App.inventory.get(i).getObject()+"' == '"+Object+"'");
                if(App.inventory.get(i).getObject().equals(Object)) {
                    if(sign) {
                        System.out.println("Cette object à +1 :"+Object);
                        App.inventory.get(i).updateQuantity(1);
                        inexistingObject = false;
                        i=App.inventory.size();
                    }else {
                        App.inventory.get(i).updateQuantity(-1);
                        inexistingObject = false;
                        i=App.inventory.size();
                    }
                }
            }
            if(inexistingObject) 
                App.inventory.add(new ObjectAdventure(Object));

        }
    }
    
    
private void interpreterPerso(String ligne) {
    	
    	
    	System.out.println("ligne Perso : "+ligne);
    	System.out.println("Perso Ajout:");
    	String attribut = ""; //PO ou PV
    	String token;
    	boolean sign = true;
    	for(int i=0; i<ligne.length(); i++) {
    		token=ligne.substring(i, i + 1);
    		if(! token.isEmpty()) {
	    		switch(Token.valueOf(token)) {
	    		case commat:
	    			persoUpdate(sign, attribut.trim());
	    			attribut ="";
	    			break;
	    		case add:
	    			sign=true;
	    			break;
	    		case minus:
	    			sign=false;
	    			break;
	    		default:
	    			attribut += token;
	    			System.out.println(attribut);
	    			break;
	    		
	    		}
	    		
    		}
    	}
    	if(!attribut.equals("")) {
    		persoUpdate(sign, attribut.trim());
    	}
    }


    
    private void persoUpdate(boolean sign, String attribut) {
	    	
    	System.out.println("Attribut Perso : "+attribut);
    	if(sign) {
    		if(attribut.equals("PV"))
    			App.player.updHealth(1); //increase PV
    		
    		if(attribut.equals("PO"))
    			App.player.updGold(1);//increase PO
    		
    	}else {
    		if(attribut.equals("PV"))
    			App.player.updHealth(-1);//decrease PV
    		
    		if(attribut.equals("PO"))
    			App.player.updGold(-1);//increase PO
    		
    	}
    }
    
    private boolean InterpréteurCondition(String ligne) {
    	
    	String object = "";
    	String token="";
    	String condition = ""+ligne;
    	boolean left = false;
    	
    	for(int i = 0; i<condition.length(); i++ ) {
    		token=condition.substring(i, i + 1);
    		if(! token.isEmpty()) {
	    		switch(Token.valueOf(token)) {
	    		case leftPar:
	    			if(! left) {
	    				left = true;
	    			}else {
	    				//trow execption
	    			}
	    			break;
	    			
	    		case rightPar:
	    			if(left) {
	    				for(int j =0; j<App.inventory.size(); j++) {
	    					object = object.trim();
	    					if(App.inventory.get(j).hasObject().equals(object)) {
		    					return true;
		    				}
		    					
	    				}
	    				return false;
	    			}else {
	    				//trow execption
	    			}
	    			break;
	    			
				default:
					if(left)
						object += token;
					break;
	    		}
    		}
    	}
    	
    	return true;
    }

    private List<Proposition> interpreterPropositions(Scanner scanner) {
        List<Proposition> propositions = new ArrayList<>();
        String choix = "";
        String intLieu = "";
        while (scanner.hasNextLine()) {
        	String token = scanner.nextLine();
            if(! token.isEmpty()) {
            	switch (Token.valueOf(token)) {
            		case finLieu:
            			return propositions;
                    
            		default:
	                   
	
	                    boolean isPropFinished = false;
	
	                    while (!isPropFinished && scanner.hasNextLine()) {
	                        String ligne = token;
	                        token = token.split(":")[0].split(" ")[0];
	                        boolean leftAccPassed = false;
	                        if(! token.isEmpty()) {    
	                        	switch (Token.valueOf(token)) {
		                            case intLieu:
		                            	intLieu = ligne.split(":")[1].trim();
		                            	Proposition prop = new Proposition(choix, Integer.parseInt(intLieu));
		        	                    propositions.add(prop);
		                            	break;
		                            case choix:
		                            	choix = ligne.split(":")[1].trim();
		                            	break;
		                            case Si:
		                            	if(Integer.parseInt(numeroLieu) == App.numeroLieuActuel){
			                            	if(! InterpréteurCondition(ligne)) {
			                                	 while (scanner.hasNextLine() && !(Token.valueOf(token) == TypeDeToken.rightAcc) ) {
			                                		 token = scanner.nextLine();
			                                	 }
			                                	break;	
			                                }else {
			                                	for(int i = 0; i<ligne.length(); i++ ) {
			                                		String tok = ligne.substring(i, i + 1);
			                                		if(! tok.isEmpty()) {
			                                			if(Token.valueOf(tok) == TypeDeToken.leftAcc) {
			                                				leftAccPassed = true;
			                                			}
			                                		}
			                                	}
			                                }
		                            	}
		                                break;
		                            case rightAcc:
		                            	if(leftAccPassed) {
		                            		leftAccPassed = false;
		                            	}else {
		                            		//trow exeption Pas de Left
		                            		
		                            	}
		                            	break;
		                            case finLieu:
		                                isPropFinished = true;
		                                break;
								
		                        }
	                        }
	                        token = scanner.nextLine();
	                    }
	
	                    return propositions;
	                    
	                
	                
	            }
        	}
        }
        return propositions;
    }

    public HashMap<Integer, Lieu> getLieux() {
        return lieux;
    }

    public void setLieux(HashMap<Integer, Lieu> lieux) {
        this.lieux = lieux;
    }

 
}
