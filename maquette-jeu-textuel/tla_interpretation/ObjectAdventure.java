package tla_interpretation;

public class ObjectAdventure {
	
	String Object;
	int Quantity;
	
	public ObjectAdventure(String name) {
		this.Object=name;
		this.Quantity=0;
	}
	
	public String hasObject() {
		if(Quantity == 0) {
			return "";
		}
		return this.Object;
	}
	
	public String getObject() {
		return this.Object;
	}
	
	public int getQuantity() {
		return this.Quantity;
	}
	
	public void updateQuantity(int update) {
		this.Quantity += update;
	}

	@Override
	public String toString() {
		return "ObjectAdventure [Object=" + Object + ", Quantity=" + Quantity + "]";
	}

	public void setQuantity(int newQuantity) {
		this.Quantity = newQuantity;
	}
	
	public String getobject() {
		return this.Object;
	}
	

}
