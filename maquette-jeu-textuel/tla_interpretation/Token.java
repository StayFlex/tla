 package tla_interpretation;

public class Token {

	private TypeDeToken typeDeToken;
	private String valeur;

	public Token(TypeDeToken typeDeToken, String value) {
		this.typeDeToken=typeDeToken;
		this.valeur=value;
	}

	public Token(TypeDeToken typeDeToken) {
		this.typeDeToken=typeDeToken;
	}

	public TypeDeToken getTypeDeToken() {
		return typeDeToken;
	}

	public String getValeur() {
		return valeur;
	}

	public String toString() {
		String res = typeDeToken.toString();
		if (valeur != null) res = res + "(" + valeur + ")";
		return res;
	}
	
	public static TypeDeToken valueOf(String line) {
		
		TypeDeToken type = null;
		
		line = line.split(":")[0];
		System.out.println(line);
		switch(line) {
			case "debutAventure" -> type = TypeDeToken.debutAventure;
			case "finAventure" -> type = TypeDeToken.finAventure;
			case "titre" -> type = TypeDeToken.titre;
			case "debutLieu" -> type = TypeDeToken.debutLieu;
			case "finLieu" -> type = TypeDeToken.finLieu;
			case "description" -> type = TypeDeToken.description;
			case "choix" -> type = TypeDeToken.choix;
			case "intLieu" -> type = TypeDeToken.intLieu;
			case "numeroLieu" -> type = TypeDeToken.numeroLieu;
			case "proposition" -> type = TypeDeToken.proposition;
			case "Si" -> type = TypeDeToken.Si;
			case "(" -> type = TypeDeToken.leftPar;
			case ")" -> type = TypeDeToken.rightPar;
			case "{" -> type = TypeDeToken.leftAcc;
			case "}" -> type = TypeDeToken.rightAcc;
			case "Perso" -> type = TypeDeToken.perso;
			case "Inventaire" -> type = TypeDeToken.inventaire;
			case "," -> type = TypeDeToken.commat;
			case "+" -> type = TypeDeToken.add;
			case "-" -> type = TypeDeToken.minus;
			default -> type = TypeDeToken.strVal;
		}
		//System.out.println(""+type+"");
		return type;
	}

}