/*

Projet TLA 2023-24

Réalisé par :
- NOM Prénom 
- NOM Prénom
- NOM Prénom
- NOM Prénom
- NOM Prénom

*/

package tla_interpretation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;


/*
 * Classe principale.
 * 
 * Gère l'IHM.
 * Affiche les lieux et propositions suivant les décisions du joueur.
 */

public class App implements ActionListener {

    // Nombre de lignes dans la zone de texte
    final int nbLignes = 20;

    String filename;
    
    Map<Integer, Lieu> lieux;
    
    public static List<ObjectAdventure> inventory =new ArrayList<ObjectAdventure>();
    
    public static Perso player = null;
    
    String titre;
    
    Lieu lieuActuel;
    
    DefaultListModel<String> listModel = new DefaultListModel<>();
    
    public static int numeroLieuActuel = 1;
    
    JPanel listPanel;
    JList<String> list;
    JScrollPane scrollPane ;
    
    JPanel statsPanel;
    
    JLabel persoLabel;
    JLabel pvLabel;
    JLabel poLabel;
    JLabel etatLabel;
    
    JSplitPane splitPane;

    JFrame frame;
    JPanel mainPanel;

    // Labels composant la zone de texte
    JLabel[] labels;

    // Boutons de proposition
    ArrayList<JButton> btns;

    public static void main(String[] args) {
        App app = new App();
        SwingUtilities.invokeLater(() -> app.init());
    }
    

    
    private void init() {
    	
    	
    	
    	

        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Veuillez écrire le nom du fichier contenant l'histoire : ");

        // Lire la ligne saisie par l'utilisateur
        this.filename = scanner.nextLine();
        
        player = new Perso();
        

        // Create a JLabel for the character's stats
        persoLabel = new JLabel("PERSO : ");
        pvLabel = new JLabel("PV : " + player.getPv());
        poLabel = new JLabel("PO : " + player.getPO());
       etatLabel = new JLabel("État : " + player.getSTATE());
        
        System.out.println("Analyse du fichier aventure");
        
       // inventory.add(new ObjectAdventure("couteau"));
        
        Textualisation textualisationInstance =  Textualisation.startAventure(this.filename);
        
        System.out.println("Contenu de l'inventaire"+inventory.get(0).toString());
    	
    	System.out.println("Lancement de l'aventure");
    	
    	
        
        lieux = textualisationInstance.getLieux();
       

        // Prépare l'IHM
        labels = new JLabel[nbLignes];
        btns = new ArrayList<>();

        frame = new JFrame(titre);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());

        frame.add(mainPanel);

        for(int i=0;i<nbLignes;i++) {
            labels[i] = new JLabel(" ");
            mainPanel.add(labels[i], new GridBagConstraints() {{
                this.gridwidth = GridBagConstraints.REMAINDER;
                this.anchor = GridBagConstraints.WEST;
                this.insets = new Insets(0,20,0,20);
            }});
            labels[i].setMinimumSize(new Dimension(750, 20));
            labels[i].setPreferredSize(new Dimension(750, 20));
        }

        // Démarre l'aventure au lieu n° 1
        lieuActuel = lieux.get(1);
        initLieu();

        frame.pack();
        frame.setVisible(true);
        persoLabel = new JLabel("PERSO : ");
        pvLabel = new JLabel("PV : " + player.getPv());
        poLabel = new JLabel("PO : " + player.getPO());
       etatLabel = new JLabel("État : " + player.getSTATE());

        // Create stats panel and add labels to it
        statsPanel = new JPanel();
        statsPanel.add(persoLabel);
        statsPanel.add(pvLabel);
        statsPanel.add(poLabel);
        statsPanel.add(etatLabel);
        statsPanel.setLayout(new BoxLayout(statsPanel, BoxLayout.Y_AXIS));

        // Create a split pane and add the listPanel and statsPanel to it
        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, listPanel, statsPanel);

        // Add the split pane to the frame
        frame.add(splitPane, BorderLayout.EAST);
    }
       
        
       

    /*
     * Affichage du lieu lieuActuel et créations des boutons de propositions correspondantes
     * à ce lieu
     */
    void initLieu() {
        for(JButton btn: btns) {
            mainPanel.remove(btn);
        }
        btns.clear();
        affiche(lieuActuel.description.split("\n"));
        frame.pack();
        updateInventoryPerso();
        for(int i=0; i<lieuActuel.propositions.size(); i++) {
            JButton btn = new JButton("<html><p>" + lieuActuel.propositions.get(i).texte + "</p></html>");
            btn.setActionCommand(String.valueOf(i));
            btn.addActionListener(this);
            mainPanel.add(btn, new GridBagConstraints() {{
                this.gridwidth = GridBagConstraints.REMAINDER;
                this.fill = GridBagConstraints.HORIZONTAL;
                this.insets = new Insets(3,20,3,20);
            }});
            btns.add(btn);
        }
        
        frame.pack();
    }

    /*
     * Gère les clics sur les boutons de propostion
     */
    public void actionPerformed(ActionEvent event) {
    	
    	

        // Retrouve l'index de la proposition
        int index = Integer.valueOf(event.getActionCommand());

        // Retrouve la propostion
        Proposition proposition = lieuActuel.propositions.get(index);

        // Recherche le lieu désigné par la proposition
        Lieu lieu = lieux.get(proposition.numeroLieu);
        if (lieu != null) {
        	numeroLieuActuel = proposition.numeroLieu;
            // Affiche la proposition qui vient d'être choisie par le joueur
            affiche(new String[]{"> " + proposition.texte});
            
            // Affichage du nouveau lieu et création des boutons des nouvelles propositions
            lieuActuel = lieu;
            
            Textualisation textualisationInstance =  Textualisation.startAventure(this.filename);
        	
        	System.out.println("Analyse du fichier aventure");
            
            lieux = textualisationInstance.getLieux();
            initLieu();
            
        } else {
            // Cas particulier : le lieu est déclarée dans une proposition mais pas encore décrit
            // (lors de l'élaboration de l'aventure par exemple)
            JOptionPane.showMessageDialog(null,"Lieu n° " + proposition.numeroLieu + " à implémenter"); 
        }
        

        // Iterate over the Inventory list and add each item to the listModel

       
      
        	   if (proposition.texte.equals("utiliser la potion")) {
        	       player.setPV(player.getPv()+5);

        	       // Find the potion in the inventory
        	       for (ObjectAdventure item : inventory) {
        	           if(item.getObject().equals("potion")) {
        	               item.updateQuantity(-1);
        	               // If the quantity of the potion reaches 0, remove it from the inventory
        	               if (item.getQuantity() == 0) {
        	                  inventory.remove(item);
        	               }
        	           }
        	       }


        	       // Call updateInventoryPerso() here to immediately reflect the changes in the inventory
        	       updateInventoryPerso();

        	       updatePvLabel(); // Update pvLabel after having updated the user interface
        	   }
        
       
       
        
        statsPanel.removeAll();
        // Recreate the labels
        persoLabel = new JLabel("PERSO : ");
        pvLabel = new JLabel("PV : " + player.getPv());
        poLabel = new JLabel("PO : " + player.getPO());
        etatLabel = new JLabel("État : " + player.getSTATE());
        // Add the labels to the statsPanel

        statsPanel.add(persoLabel);
        statsPanel.add(pvLabel);
        statsPanel.add(poLabel);
        statsPanel.add(etatLabel);
        statsPanel.setLayout(new BoxLayout(statsPanel, BoxLayout.Y_AXIS));
        // Update the statsPanel
        statsPanel.revalidate();
        statsPanel.repaint();
        frame.pack();
     }
    
        	
        
        
    

    public void updateInventoryPerso() {
    	 listModel.clear();;
    	// Create a JList with the model
         list = new JList<>(listModel);

         // Add the JList to a JScrollPane
         scrollPane = new JScrollPane(list);

         // Create a JPanel and add the JScrollPane to it
         listPanel = new JPanel();
         listPanel.add(scrollPane);

         // Iterate over the Inventory list and add each item to the listModel
         for (ObjectAdventure item : inventory) {
         	System.out.println("Ajout de l item: "+item.getobject() + " (" + item.getQuantity() + ")");
         	if(item.getQuantity() > 0) 
         		listModel.addElement(item.getobject() + " (" + item.getQuantity() + ")");
         
         	
       
    	
            	
        	
        }

        
    }
       
    /*
     * Gère l'affichage dans la zone de texte, avec un effet de défilement
     * (comme dans un terminal)
     */
    private void affiche(String[] contenu) {
        int n = contenu.length;
        for (int i = 0; i < nbLignes-(n+1); i++) {
            labels[i].setText(labels[i + n + 1].getText());
        }
        labels[nbLignes-(n+1)].setText(" ");
        for(int i = 0; i<n; i++) {
            labels[nbLignes-n+i].setText(contenu[i]);
        }
    }
    
    public void updatePvLabel() {
    	   pvLabel.setText("PV : " + player.getPv());
    	   statsPanel.revalidate();
    	   statsPanel.repaint();
    	}


}
